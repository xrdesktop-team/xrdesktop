option('api_doc',
  type: 'boolean',
  value: false,
  description: 'Build API documentation using GTK-Doc'
)

option('renderdoc',
  type: 'boolean',
  value: false,
  description: 'Enable debugging XrdSceneRender with renderdoc'
)

option('introspection', type : 'boolean', value : false)
